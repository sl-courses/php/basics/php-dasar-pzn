<?php
$a = 10;
$b = 5;
//Operasi penambahan
echo "===Operasi penambahan===\n";
echo $a + $b;
echo "\n";

//Operasi pengurangan
echo "===Operasi pengurangan===\n";
echo $a - $b;
echo "\n";

//Operasi perkalian
echo "===Operasi perkalian===\n";
echo $a * $b;
echo "\n";

//Operasi pembagian
echo "===Operasi pembagian===\n";
echo $a / $b;
echo "\n";

//Operasi modulus
echo "===Operasi modulus===\n";
echo $a % $b;
echo "\n";

//Operasi pangkat
echo "===Operasi pangkat===\n";
echo $a ** $b;
echo "\n";

?>