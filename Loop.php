<?php

//Perulangan tanpa henti
// for (; ;) {
//     echo "Hello World";
//     echo "\n";
// }

//Perulangan dengan kondisi
echo "===Perulangan dengan kondisi===\n";
for ($i = 0; $i < 10; $i++) {
    echo "Hello World";
    echo "\n";
}

//Perulangan dengan init statement
echo "===Perulangan dengan init statement===\n";
for ($i = 1; $i < 10;) :
    echo "Hello World";
    echo "\n";
    $i++;
endfor;

//while loop
echo "===While Loop===\n";
$i = 0;
while ($i < 10) {
    echo "Hello World";
    echo "\n";
    $i++;
}

//do while loop
echo "===Do While Loop===\n";
$i = 0;
do {
    echo "Hello World";
    echo "\n";
    $i++;
} while ($i < 10);

//foreach loop
echo "===Foreach Loop===\n";
$colors = array("red", "green", "blue", "yellow");
foreach ($colors as $value) {
    echo "$value";
    echo "\n";
}

//foreach loop with key
echo "===Foreach Loop with Key===\n";
$colors = array("a" => "red", "b" => "green", "c" => "blue", "d" => "yellow");
foreach ($colors as $key => $value) {
    echo "$key = $value";
    echo "\n";
}
