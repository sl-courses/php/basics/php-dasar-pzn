<?php
//IF Statement
$a = 10;
$b = 5;
if ($a > $b) {
    echo "a lebih besar dari b";
}
echo "\n";
//IF ELSE Statement
if ($a < $b) {
    echo "a lebih kecil dari b";
} else {
    echo "a lebih besar dari b";
}
echo "\n";
//IF ELSEIF ELSE Statement
if ($a < $b) {
    echo "a lebih kecil dari b";
} elseif ($a == $b) {
    echo "a sama dengan b";
} else {
    echo "a lebih besar dari b";
}
echo "\n";
//IF Statement dengan operator logika and
if ($a > $b && $a == 10) {
    echo "a lebih besar dari b dan a sama dengan 10";
}
echo "\n";
//IF Statement dengan operator logika or
if ($a > $b || $a == 10) {
    echo "a lebih besar dari b atau a sama dengan 10";
}
echo "\n";
//IF Statement dengan operator logika xor
if ($a > $b xor $a == 10) {
    echo "a lebih besar dari b atau a sama dengan 10, tapi tidak keduanya";
}
echo "\n";
//IF Statement dengan operator logika not
if (!($a > $b)) {
    echo "a lebih kecil dari b";
}
echo "\n";
//IF Statement dengan operator logika not
if (!($a > $b)) {
    echo "a lebih kecil dari b";
}
echo "\n";
//IF Statement dengan operator logika not
if (!($a > $b)) {
    echo "a lebih kecil dari b";
}
echo "\n";
//IF Statement dengan operator logika not
if (!($a > $b)) {
    echo "a lebih kecil dari b";
}
echo "\n";
?>