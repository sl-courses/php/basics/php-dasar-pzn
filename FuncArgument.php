<?php
//Function argument
//Argument adalah nilai yang kita berikan saat memanggil function
//Contoh:
echo "Contoh function Argument : \n";
function sayHello($name, $lastName = "") //default argumemnt pada lastName
{
    echo "Hello $name $lastName" . PHP_EOL;
}
sayHello("Dian", "Prayoga");
echo "\n";
//Fixed data type
//Kita juga bisa menentukan tipe data dari argument
//Contoh:
echo "Contoh function fixed data type : \n";
function sum(int $first, int $last)
{
    $total = $first + $last;
    return $total;
}
echo sum(100, 100);
echo "\n";
echo sum("100", "100"); //PHP akan mengkonversi String ke Int secara otomatis
echo "\n";
echo sum(true, false); //Boolean juga dikonversi coyyy
echo "\n\n";
//Variable length argument
//Kita juga bisa membuat function yang memiliki jumlah argument yang tidak terbatas
//Contoh:
echo "Contoh function variable length argument : \n";
function sumAll(...$values)
{
    $total = 0;
    foreach ($values as $value) {
        $total += $value;
    }
    echo "Total " . implode(",", $values) . " = $total" . PHP_EOL;
}
echo sumAll(1, 2, 3, 4, 5, 6, 7, 8, 9, 10);
echo "\n";
//Return type declarations
//Kita juga bisa menentukan tipe data dari nilai yang dikembalikan oleh function
//Contoh:
echo "Contoh function return type declarations : \n";
function sum2(int $first, int $last): int
{
    $total = $first + $last;
    return $total;
}
echo sum2(100, 100);
echo "\n";
?>