<?php
$a = true;
$b = false;
//Operator and
echo "===Operator &&===\n";
var_dump($a && $b);
echo "\n";
echo "===Operator and===\n";
var_dump($a and $b);
echo "\n";
//Operator or
echo "===Operator ||===\n";
var_dump($a || $b);
echo "\n";
//Operator xor
//Salah satu true, tapi tidak keduanya
echo "===Operator xor===\n";
var_dump($a xor $b);
echo "\n";
//Operator not
echo "===Operator !===\n";
var_dump(!$a);
echo "\n";
?>
