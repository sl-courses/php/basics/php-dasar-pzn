<?php
$nama = "Dian";
$nama2 = "Saputra";
define("xx", "\n");
echo xx;
echo "Nama : " . $nama . PHP_EOL;
//variable parsing
echo "===Variable parsing===\n";
echo "Nama : $nama $nama2 ". xx;
//curly brace
echo "===Curly brace===\n";
echo "Nama : {$nama}a {$nama2}ni ". xx;
//Konversi ke number dan sebaliknya
$angka = 10;
$huruf = "10";
echo xx;
echo "===Konversi ke number===\n";
var_dump($angka);
$angka = (string) 10;
var_dump($angka);
echo xx;
echo "===Konversi ke string===\n";
var_dump($huruf);
$huruf = (int) "10";
var_dump($huruf);
echo xx;

//Mengakses string
echo "===Mengakses string===\n";
echo $nama[0];
echo xx;


?>