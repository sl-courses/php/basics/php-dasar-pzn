<?php
// Operator sama dengan
echo "===Operator Sama Dengan===\n";
$a = 10;
$b = 10;
var_dump($a == $b);
echo "\n";
// Operator tidak sama dengan
echo "===Operator Tidak Sama Dengan===\n";
$a = 10;
$b = 5;
var_dump($a != $b);
// atau
var_dump($a <> $b);
echo "\n";
// Operator lebih besar dari
echo "===Operator Lebih Besar Dari===\n";
$a = 10;
$b = 5;
var_dump($a > $b);
echo "\n";
// Operator lebih kecil dari
echo "===Operator Lebih Kecil Dari===\n";
$a = 10;
$b = 5;
var_dump($a < $b);
echo "\n";
// Operator lebih besar sama dengan
echo "===Operator Lebih Besar Sama Dengan===\n";
$a = 10;
$b = 10;
var_dump($a >= $b);
echo "\n";
// Operator lebih kecil sama dengan
echo "===Operator Lebih Kecil Sama Dengan===\n";
$a = 10;
$b = 10;
var_dump($a <= $b);
echo "\n";
// Operator identik
echo "===Operator Identik===\n";
$a = 10;
$b = "10";
var_dump($a === $b);
echo "\n";
// Operator tidak identik
echo "===Operator Tidak Identik===\n";
$a = 10;
$b = "10";
var_dump($a !== $b);
echo "\n";

?>