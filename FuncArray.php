<?php 
// Array Function
// Array function adalah function yang digunakan untuk memanipulasi array
// Contoh:
echo "Contoh array function : \n";
// count() : menghitung jumlah elemen array
echo count(["Dian", "Dayeeen"]) . PHP_EOL;
// sort() : mengurutkan elemen array
$names = ["Dian", "Dayeeen", "Ari", "Ayu"];
sort($names);
print_r($names);
echo PHP_EOL;
// rsort() : mengurutkan elemen array secara descending
$names = ["Dian", "Dayeeen", "Ari", "Ayu"];
rsort($names);
print_r($names);
echo PHP_EOL;
// in_array() : mencari elemen array
var_dump(in_array("Dian", ["Dian", "Dayeeen", "Ari", "Ayu"]));
echo PHP_EOL;
// array_search() : mencari elemen array dan mengembalikan indexnya
echo array_search("Dian", ["Dian", "Dayeeen", "Ari", "Ayu"]);
echo PHP_EOL;
// array_merge() : menggabungkan 2 array
print_r(array_merge(["Dian", "Dayeeen"], ["Ari", "Ayu"]));
echo PHP_EOL;
// explode() : memecah string menjadi array
print_r(explode(" ", "Dian Dayeeen"));
echo PHP_EOL;
// implode() : menggabungkan array menjadi string
print_r(implode(" ", ["Dian", "Dayeeen"]));

//apa itu print_r dan var_dump
//print_r adalah fungsi untuk mencetak array
//var_dump adalah fungsi untuk mencetak array dan tipe data

?>