<?php
// Tipe data number
echo "\n===Tipe Data Number===\n";
var_dump(123);
var_dump(0123);
var_dump(0x1A);
var_dump(1.234);
var_dump(10.2e3);

// Tipe data boolean
echo "\n===Tipe Data Boolean===\n";
var_dump(true);
var_dump(false);

// Tipe data string
echo "\n===Tipe Data String===\n";
var_dump("Hello world");
echo <<<TAG
Metode heredoc
Multiline

TAG;

echo <<<'TAG'
Metode nowdoc
metode ini tidak bisa parsing variable
Multiline

TAG;
?>