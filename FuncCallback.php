<?php
//Callback function
//Callback function adalah function yang kita kirimkan ke function lain sebagai parameter
//Contoh:
echo "Contoh callback function : \n";
function sayHello(string $name, callable $filter)
{
    $finalName = call_user_func($filter, $name);
    // $filter($name); <== sama kayak gitu aslinya
    echo "Hello $finalName" . PHP_EOL;
}
sayHello("Dayeeen", "strtoupper");
sayHello("Dayeeen", "strtolower");
sayHello("Dayeeen", function (string $name): string {
    return strtoupper($name);
});
sayHello("Dayeeen", fn (string $name) => strtoupper($name));
?>