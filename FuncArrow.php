<?php
//Arrow Function (PHP 7.4)
//Arrow function adalah anonymouse function yang lebih sederhana
//Di arrow function bisa menggunakan variable di luar closure secara otomatis
//Contoh:
echo "Contoh arrow function : \n";
$hello = fn (string $name) => "Hello $name" . PHP_EOL;
echo $hello("Dayeeen");
function sayGoodbye(string $name, $filter)
{
    $finalName = $filter($name);
    echo "Goodbye $finalName" . PHP_EOL;
}
sayGoodbye("Dayeeen", fn (string $name) => strtoupper($name));
$filterfunction = fn (string $name) => strtoupper($name);
sayGoodbye("Dayeeen", $filterfunction);

?>