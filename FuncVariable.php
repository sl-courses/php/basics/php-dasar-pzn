<?php
//Variable Function
//Kita juga bisa membuat function yang memiliki nama yang dinamis
//Contoh:
echo "Contoh variable function : \n";
function sayHello($name = "Guest")
{
    echo "Hello $name" . PHP_EOL;
}
function sayGoodBye()
{
    echo "Good Bye" . PHP_EOL;
}
$functionVariable = "sayHello";
$functionVariable("Dian");
$functionVariable = "sayGoodBye";
$functionVariable();

function sayFuckU(string $name, $filter)
{
    $finalName = $filter($name);
    echo "Fuckkk youuu $finalName" . PHP_EOL;
}
sayFuckU("Dayeeen", "strtoupper");
sayFuckU("Dayeeen", "strtolower");
sayFuckU("Dayeeen", "ucwords");
sayFuckU("Dayeeen", "ucfirst");
sayFuckU("Dayeeen", "strrev");

?>