<?php
//switch
$nilai = 80;
switch ($nilai) {
    case 100:
        echo "Sangat Baik";
        break;
    case 90:
        echo "Baik";
        break;
    case 80:
        echo "Cukup";
        break;
    case 70:
        echo "Kurang";
        break;
    default:
        echo "Sangat Kurang";
        break;
}
?>