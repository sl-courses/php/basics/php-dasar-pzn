<?php 
// Recursive Function
// Recursive function adalah function yang memanggil dirinya sendiri
// Contoh:
echo "Contoh recursive function : \n";
function factorial(int $n): int
{
    if ($n <= 1) {
        return 1;
    } else {
        return $n * factorial($n - 1);
    }
}
echo factorial(5) . PHP_EOL;
// Contoh lain:
function fibonacci(int $n): int
{
    if ($n <= 1) {
        return $n;
    } else {
        return fibonacci($n - 1) + fibonacci($n - 2);
    }
}
echo fibonacci(5) . PHP_EOL;
// Contoh lain:
function sum(int $n): int
{
    if ($n <= 1) {
        return $n;
    } else {
        return $n + sum($n - 1);
    }
}
echo sum(5) . PHP_EOL;
// Contoh lain:
function sumEven(int $n): int
{
    if ($n <= 1) {
        return $n;
    } else {
        if ($n % 2 == 0) {
            return $n + sumEven($n - 2);
        } else {
            return sumEven($n - 1);
        }
    }
}
echo sumEven(5) . PHP_EOL;
// Contoh lain:
function sumOdd(int $n): int
{
    if ($n <= 1) {
        return $n;
    } else {
        if ($n % 2 == 1) {
            return $n + sumOdd($n - 2);
        } else {
            return sumOdd($n - 1);
        }
    }
}
echo sumOdd(5) . PHP_EOL;
?>