<?php 
// Reference di PHP tidak sama dengan C/C++
// Pada PHP, reference adalah sebuah variable yang menunjuk ke variable lain
// Contoh:
$variable = "Dian";
$reference = &$variable;
$reference = "Dayeeen";
echo $variable;
echo PHP_EOL;
echo $reference;
echo PHP_EOL;

// Pass By Reference
// Pada PHP, function dapat menerima parameter berupa reference
// Contoh:
function increment(int &$value) {
    $value++;
}
$counter = 1;
increment($counter);
echo $counter;
echo PHP_EOL;

// Return By Reference
// Pada PHP, function dapat mengembalikan nilai berupa reference
// Contoh:
function &getValue() {
    static $value = 100;
    return $value;
}
$value = &getValue();
$value = 200;
echo getValue();
echo PHP_EOL;

?>