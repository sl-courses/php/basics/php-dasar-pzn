<?php
//Anonymouse function atau Closure
//Anonymouse function adalah function yang tidak memiliki nama
//Contoh:
echo "Contoh anonymouse function : \n";
$hello = function (string $name) {
    echo "Hello $name" . PHP_EOL;
};
$hello("Dayeeen");

function sayGoodbye(string $name, $filter) {
    $finalName = $filter($name);
    echo "Goodbye $finalName" . PHP_EOL;
}
sayGoodbye("Dayeeen", function (string $name): string {
    return strtoupper($name);
});
$filterfunction = function (string $name) : string {
    return strtoupper($name);
};
sayGoodbye("Dayeeen", $filterfunction);

//Mengakses variable di luar closure/anonymous
//Kita bisa mengakses variable di luar closure/anonymous function
//Ketika kita membuat closure/anonymous function, kita bisa menggunakan keyword use
//Contoh:
echo "Contoh mengakses variable di luar closure/anonymous function : \n";
$firstName = "Dian";
$lastName = "Prayoga";
$fullName = function () use ($firstName, $lastName) {
    echo "Hello $firstName $lastName" . PHP_EOL;
};
?>