<?php 
// String Function
// String function adalah function yang digunakan untuk memanipulasi string
// Contoh:
echo "Contoh string function : \n";
// strlen() : menghitung panjang string
echo strlen("Dian") . PHP_EOL;
// strcmp() : membandingkan 2 string
echo strcmp("Dian", "Dian") . PHP_EOL;
echo strcmp("Dian", "Dayeeen") . PHP_EOL;
echo strcmp("Dayeeen", "Dian") . PHP_EOL;
// explode() : memecah string menjadi array
print_r(explode(" ", "Dian Dayeeen"));
echo PHP_EOL;
// implode() : menggabungkan array menjadi string
print_r(implode(" ", ["Dian", "Dayeeen"]));
echo PHP_EOL;
// trim() : menghapus spasi di awal dan akhir string
echo trim(" Dian Dayeeen ");
echo PHP_EOL;
// strtolower() : mengubah string menjadi huruf kecil
echo strtolower("Dian Dayeeen");
echo PHP_EOL;
// strtoupper() : mengubah string menjadi huruf besar
echo strtoupper("Dian Dayeeen");
echo PHP_EOL;
// str_replace() : mengganti string dengan string lain
echo str_replace("Dian", "Dayeeen", "Dian Dayeeen");
?>