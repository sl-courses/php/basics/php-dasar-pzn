<?php
// Function
//Function adalah sebuah blok kode yang akan dijalankan ketika dipanggil
echo "Contoh function 1 : \n";
function sayHello($name = "Guest")
{
    echo "Hello $name" . PHP_EOL;
}
sayHello("Dian");
sayHello();
//Di PHP, kita bisa meletakan function dimana pun
//Karena PHP akan menganggap function sebagai statement
//Bahkan kita bisa membuat function dalam if statement, di dalam function, dsb
//Contoh:
echo "Contoh function 2 : \n";
function sayHello2($name = "Guest")
{
    echo "Hello $name" . PHP_EOL;
    function sayGoodBye()
    {
        echo "Good Bye" . PHP_EOL;
    }
    sayGoodBye();
}
sayHello2("Dian");
//Kita juga bisa membuat function yang mengembalikan nilai
//Contoh:
echo "Contoh function 3 : \n";
function sum(int $first, int $last): int
{
    $total = $first + $last;
    return $total;
}
echo sum(100, 100);


?>