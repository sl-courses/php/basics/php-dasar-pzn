<?php 
// Variable Scope
// Variable Scope adalah variable yang hanya bisa diakses di dalam function tertentu

//Global scope
//Variable yang hanya bisa diakses di luar function
$nama = "GLobal Scope"; //Global Scope

function sayHello() {
    // echo "Hello $nama" . PHP_EOL; //error
}
sayHello();

//Local scope
//Variable yang hanya bisa diakses di dalam function
function sayHello2() {
    $nama2 = "Local Scope"; //Local Scope
    echo "Hello $nama2" . PHP_EOL;
}
sayHello2();

//Global keyword
//Keyword yang digunakan untuk mengakses variable global di dalam function
function sayHello3() {
    global $nama;
    echo "Hello $nama" . PHP_EOL;
}
sayHello3();

//Static Scope
//Variable yang hanya bisa diakses di dalam function dan nilainya tetap
function increment() {
    static $counter = 1;
    echo "Counter : $counter" . PHP_EOL;
    $counter++;
}
increment();
increment();
increment();

//Super global
//Variable global yang sudah disediakan oleh PHP
//Contoh:
//$_GET
//$_POST
//$_REQUEST
//$_SERVER
//$_SESSION
//$_COOKIE
//$_ENV
//$_FILES
//$_GLOBALS

?>