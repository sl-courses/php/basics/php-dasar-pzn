<?php

// Variable
echo "\n===Variable===\n";
$Dian = "Dayen";
echo "Hello $Dian\n";
$Age = 20;
echo "Age $Age\n";
// bisa ganti dari int ke string tanpa casting. 
// begitupun dari string ke int
$Age = "Dua puluh";
echo "Age $Age\n";

// variable variables
echo "\n===Variable Variables===\n";
$Nama = "Dian";
$$Nama = "Saputra";
echo "$Nama $Dian\n";

//Constant
echo "\n===Constant===\n";
define("CODER", "Dayen\n");
define("PI", 3.14);
echo CODER;
echo PI;
echo "\n";

//DATA NULL
echo "\n===DATA NULL===\n";
$var = "DAYEN";
$var = null;
var_dump($var);
var_dump(is_null($var));
echo is_null($var);

//Menghapus variable
echo "\n===Menghapus variable===\n";
$var = "DAYEN";
unset($var);
// better use isset karena lebih masuk akal
var_dump(isset($var));
isset($var);
echo isset($var);

?>