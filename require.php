<?php 
//Require
//Require adalah function yang digunakan untuk memanggil file lain
//Pada require, jika file yang dipanggil tidak ada, maka akan muncul error
//Contoh:
require "test.php";
//Include
//Pada include, jika file yang dipanggil tidak ada, maka akan muncul warning
//Contoh:
include "test.php";
?>