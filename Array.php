<?php
// Tipe data array

// Array di php bersifat dinamis
echo "\n===Tipe Data Array===\n";
// OPERASI ARRAY
echo "\n===OPERASI ARRAY===\n";
$var = array("Dian", 20, array("Saputra", "Dayen"));
var_dump($var);

echo $var[2][0][6]; // array ke dua index 0 = saputra
//index 6 = a
echo "\n";

//Mengubah array
$var[2][0] = "Dayen";
var_dump($var);

//Menambah array
$var[] = "Dian";
var_dump($var);

//Menghapus array
unset($var[0]);
var_dump($var);
//Array yang dihapus tidak menyebabkan pergeseran index
//Array yang dihapus hanya mengubah nilainya menjadi null

//Menghitung jumlah array
echo "Jumlah array : ";
echo count($var);
echo "\n";

//Menghitung jumlah array dengan index
echo count($var, COUNT_RECURSIVE);
//count recursive dipakai untuk menghitung jumlah array secara keseluruhan
echo "\n";

//Menghitung jumlah array dengan index
echo count($var, COUNT_NORMAL);
//count normal dipakai untuk menghitung jumlah array secara normal
echo "\n";

//Array sebagai Map
echo "\n===Array sebagai Map===\n";
$Dian = array(
    "nama" => "Dian",
    "umur" => 20,
    "alamat" => "Jl. Cendana"
);
var_dump($Dian);
echo $Dian["umur"];
echo "\n";

// atau bisa juga dengan komposisi berikut
$Dayen = [
    "nama" => "Dayen",
    "umur" => 20,
    "alamat" => [
        "jalan" => "Jl. Cendana",
        "kota" => "Bandung"
    ]
];
echo $Dayen["alamat"]["kota"];
echo "\n"; 
?>