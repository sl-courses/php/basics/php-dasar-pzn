<?php
//Union
$a = [
    "a" => "red",
    "b" => "green",
];
$b = [
    "c" => "blue",
    "d" => "yellow",
];
$c = $a + $b;
var_dump($c);
echo "\n";

//Intersection
$a = [
    "a" => "red",
    "b" => "green",
    "c" => "blue",
    "d" => "yellow",
];
$b = [
    "c" => "blue",
    "d" => "yellow",
    "e" => "purple",
];
$c = array_intersect($a, $b);
var_dump($c);
echo "\n";

//Difference
$a = [
    "a" => "red",
    "b" => "green",
    "c" => "blue",
    "d" => "yellow",
];
$b = [
    "c" => "blue",
    "d" => "yellow",
    "e" => "purple",
];
$c = array_diff($a, $b);
var_dump($c);
echo "\n";

//Symmetric Difference
$a = [
    "a" => "red",
    "b" => "green",
    "c" => "blue",
    "d" => "yellow",
];
$b = [
    "c" => "blue",
    "d" => "yellow",
    "e" => "purple",
];
$c = array_diff($a, $b) + array_diff($b, $a);
var_dump($c);
echo "\n";

//Array Merge
$a = [
    "a" => "red",
    "b" => "green",
];
$b = [
    "c" => "blue",
    "d" => "yellow",
];
$c = array_merge($a, $b);
var_dump($c);
echo "\n";

//Array Merge Recursive
$a = [
    "a" => "red",
    "b" => "green",
    "c" => [
        "d" => "blue",
        "e" => "yellow",
    ],
];
$b = [
    "c" => [
        "f" => "purple",
        "g" => "orange",
    ],
    "h" => "pink",
];
$c = array_merge_recursive($a, $b);
var_dump($c);
echo "\n";

?>